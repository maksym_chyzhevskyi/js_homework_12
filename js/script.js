"use strict";
/*Чому для роботи з input не рекомендується використовувати клавіатуру?
Тому що користувач може ввести певну інформацію не лище за допомгою клавітуари,
а також іншим чином: наприклад через праву клавішу миші + "Вставити", голосом тощо.
Тому, краще використовувати метод "input".
*/

let button = document.querySelectorAll(".btn");
document.body.addEventListener("keypress", pressButton)

function pressButton(event) {
    for (let element of button) {
        if (element.innerText.toLowerCase() === event.key.toLowerCase()) {
            element.classList.add("btn-active");
        } else {
            element.classList.remove("btn-active")
        }
    }
}
